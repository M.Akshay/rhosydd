/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <glib.h>
#include <glib/gi18n-lib.h>
#include <gio/gio.h>
#include "libinternal/arrays.h"
#include "libinternal/logging.h"
#include "libinternal/timestamped-pointer.h"
#include "librhosydd/subscription.h"
#include "librhosydd/subscription-manager.h"
#include "librhosydd/types.h"
#include "librhosydd/vehicle.h"
#include "aggregate-vehicle.h"


static void vdd_aggregate_vehicle_vehicle_init (RsdVehicleInterface *iface);
static void vdd_aggregate_vehicle_dispose      (GObject *object);

static void vdd_aggregate_vehicle_get_property (GObject    *object,
                                                guint       property_id,
                                                GValue     *value,
                                                GParamSpec *pspec);
static void vdd_aggregate_vehicle_set_property (GObject      *object,
                                                guint         property_id,
                                                const GValue *value,
                                                GParamSpec   *pspec);

static const gchar     *vdd_aggregate_vehicle_vehicle_get_id                    (RsdVehicle          *vehicle);

static void             vdd_aggregate_vehicle_vehicle_set_attributes_async      (RsdVehicle          *vehicle,
                                                                                 GHashTable    *attribute_values,
                                                                                 GCancellable        *cancellable,
                                                                                 GAsyncReadyCallback  callback,
                                                                                 gpointer             user_data);

static void             vdd_aggregate_vehicle_vehicle_set_attributes_finish     (RsdVehicle          *vehicle,
                                                                                 GAsyncResult        *result,
                                                                                 GError             **error);

static void             vdd_aggregate_vehicle_vehicle_get_attributes_async  (RsdVehicle          *vehicle,
                                                                             const gchar         *node,
                                                                             GCancellable        *cancellable,
                                                                             GAsyncReadyCallback  callback,
                                                                             gpointer             user_data);

static GPtrArray       *vdd_aggregate_vehicle_vehicle_get_attributes_finish (RsdVehicle          *vehicle,
                                                                             GAsyncResult        *result,
                                                                             RsdTimestampMicroseconds *current_time,
                                                                             GError             **error);

static void             vdd_aggregate_vehicle_vehicle_get_metadata_async    (RsdVehicle          *vehicle,
                                                                             const gchar         *node,
                                                                             GCancellable        *cancellable,
                                                                             GAsyncReadyCallback  callback,
                                                                             gpointer             user_data);

static GPtrArray       *vdd_aggregate_vehicle_vehicle_get_metadata_finish   (RsdVehicle          *vehicle,
                                                                             GAsyncResult        *result,
                                                                             RsdTimestampMicroseconds *current_time,
                                                                             GError             **error);

static void             vdd_aggregate_vehicle_vehicle_update_subscriptions_async  (RsdVehicle          *vehicle,
                                                                                   GPtrArray           *subscriptions,
                                                                                   GPtrArray           *unsubscriptions,
                                                                                   GCancellable        *cancellable,
                                                                                   GAsyncReadyCallback  callback,
                                                                                   gpointer             user_data);

static void             vdd_aggregate_vehicle_vehicle_update_subscriptions_finish (RsdVehicle          *vehicle,
                                                                                   GAsyncResult        *result,
                                                                                   GError             **error);

const RsdAttribute      *vdd_aggregate_vehicle_get_attribute                  (VddAggregateVehicle         *self,
                                                                               const gchar                 *attribute_name,
                                                                               const RsdAttributeMetadata  **metadata);

GPtrArray               *vdd_aggregate_vehicle_get_attributes                 (VddAggregateVehicle           *self,
                                                                               char                          *node_path);

GPtrArray               *vdd_aggregate_vehicle_get_metadata                   (VddAggregateVehicle           *self,
                                                                               char                          *node_path);

void                    vdd_aggregate_vehicle_clear_attributes                (VddAggregateVehicle           *self);

void                    vdd_aggregate_vehicle_add_attribute                   (VddAggregateVehicle        *self,
                                                                               RsdAttributeInfo           *attribute_info,
                                                                               gint64                     clock_delta);
/**
 * CacheValidity:
 * @CACHE_VALUES_VALID: Whether the values of the attributes in the cache are
 *    up to date.
 * @CACHE_STRUCTURE_VALID: Whether the hierarchy of attribute names
 *    in the cache is up to date.
 *
 * Tracking the validity of various parts of the attribute cache.
 */
typedef enum
{
  CACHE_NOT_VALID = 0,
  CACHE_VALUES_VALID = (1 << 0),
  CACHE_STRUCTURE_VALID = (1 << 1),
} CacheValidity;

#define CACHE_VALID (CACHE_VALUES_VALID | CACHE_STRUCTURE_VALID)

/**
 * VddAggregateVehicle:
 *
 * An implementation of #RsdVehicle which aggregates the attributes 
 * from multiple other vehicles.
 *
 * Attributes are retrieved and set  in the aggregate vehicle
 *
 * The clock domains of the various vehicles are aggregated, with the
 * #VddAggregateVehicle tracking a delta between its current clock time
 * (g_get_monotonic_time()) and the clock for each vehicle. This delta is used
 * to adjust the reported update time for each attribute so they are all in the
 * aggregated clock domain. However, if an attribute has an update time of
 * %RSD_TIMESTAMP_UNKNOWN, it is not modified.
 *
 * Since: 0.1.0
 */
struct _VddAggregateVehicle
{
  GObject parent;

  gchar *id;  /* owned */
  GPtrArray/*<owned RsdVehicle>*/ *vehicles;  /* owned */
  GPtrArray /*<owned RsdAttributeInfo>*/ *attributes;
  GHashTable/*<unowned RsdVehicle, owned gint64*>*/ *clock_deltas;  /* owned */

  CacheValidity cache_validity;
  GTask *cache_update_task;  /* owned; nullable if no update ongoing */

  RsdSubscriptionManager *subscriptions;  /* owned */
};

typedef enum
{
  PROP_ID = 1,
} VddAggregateVehicleProperty;

G_DEFINE_TYPE_WITH_CODE (VddAggregateVehicle, vdd_aggregate_vehicle,
                         G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (RSD_TYPE_VEHICLE,
                                                vdd_aggregate_vehicle_vehicle_init))

static void
vdd_aggregate_vehicle_class_init (VddAggregateVehicleClass *klass)
{
  GObjectClass *object_class = (GObjectClass *) klass;

  object_class->get_property = vdd_aggregate_vehicle_get_property;
  object_class->set_property = vdd_aggregate_vehicle_set_property;
  object_class->dispose = vdd_aggregate_vehicle_dispose;

  g_object_class_override_property (object_class, PROP_ID, "id");
}

static void
vdd_aggregate_vehicle_vehicle_init (RsdVehicleInterface *iface)
{
  iface->get_id = vdd_aggregate_vehicle_vehicle_get_id;
  iface->get_attributes_async = vdd_aggregate_vehicle_vehicle_get_attributes_async;
  iface->get_attributes_finish = vdd_aggregate_vehicle_vehicle_get_attributes_finish;
  iface->get_metadata_async = vdd_aggregate_vehicle_vehicle_get_metadata_async;
  iface->get_metadata_finish = vdd_aggregate_vehicle_vehicle_get_metadata_finish;
  iface->set_attributes_async = vdd_aggregate_vehicle_vehicle_set_attributes_async;
  iface->set_attributes_finish = vdd_aggregate_vehicle_vehicle_set_attributes_finish;
  iface->update_subscriptions_async = vdd_aggregate_vehicle_vehicle_update_subscriptions_async;
  iface->update_subscriptions_finish = vdd_aggregate_vehicle_vehicle_update_subscriptions_finish;
}

static void
vdd_aggregate_vehicle_init (VddAggregateVehicle *self)
{
  /* We can intern the hash keys for the table, but we must use string
   * hash functions rather than direct hash functions, as we must not intern
   * arbitrary keys on lookup (only on insertion). Otherwise we would allow an
   * attacker to cause unbounded allocations in the daemon by making repeated
   * requests for bogus attributes */
  self->vehicles = g_ptr_array_new_with_free_func (g_object_unref);

  self->clock_deltas = g_hash_table_new_full (g_direct_hash, g_direct_equal,
                                              NULL, g_free);
  self->attributes = g_ptr_array_new_with_free_func (
                       (GDestroyNotify)rsd_attribute_info_free);

  self->cache_validity = CACHE_NOT_VALID;

  self->subscriptions = rsd_subscription_manager_new ();
}

static void
vdd_aggregate_vehicle_dispose (GObject *object)
{
  VddAggregateVehicle *self = VDD_AGGREGATE_VEHICLE (object);
  gsize i;

  for (i = 0; self->vehicles != NULL && i < self->vehicles->len; i++)
    {
      RsdVehicle *vehicle = self->vehicles->pdata[i];
      g_signal_handlers_disconnect_by_data (vehicle, self);
    }

  g_clear_pointer (&self->vehicles, g_ptr_array_unref);
  g_clear_pointer (&self->attributes, g_ptr_array_unref);
  g_clear_pointer (&self->clock_deltas, g_hash_table_unref); 
  g_clear_object (&self->subscriptions);
  g_clear_pointer (&self->id, g_free);

  /* Chain up to the parent class */
  G_OBJECT_CLASS (vdd_aggregate_vehicle_parent_class)->dispose (object);
}

static void
vdd_aggregate_vehicle_get_property (GObject    *object,
                                    guint       property_id,
                                    GValue     *value,
                                    GParamSpec *pspec)
{
  VddAggregateVehicle *self = VDD_AGGREGATE_VEHICLE (object);

  switch ((VddAggregateVehicleProperty) property_id)
    {
    case PROP_ID:
      g_value_set_string (value, self->id);
      break;
    default:
      g_assert_not_reached ();
    }
}

static void
vdd_aggregate_vehicle_set_property (GObject      *object,
                                    guint         property_id,
                                    const GValue *value,
                                    GParamSpec   *pspec)
{
  VddAggregateVehicle *self = VDD_AGGREGATE_VEHICLE (object);

  switch ((VddAggregateVehicleProperty) property_id)
    {
    case PROP_ID:
      /* Construct only. */
      g_assert (self->id == NULL);
      g_assert (rsd_vehicle_id_is_valid (g_value_get_string (value)));
      self->id = g_value_dup_string (value);
      break;
    default:
      g_assert_not_reached ();
    }
}

/*
 * find_aggregate_attribute:
 * @self: a #VddAggregateVehicle
 * @attribute_name: name of the attribute
 * @metadata: (out caller-allocates) (optional): return location for the
 *    attribute metadata
 * @error: return location for a #GError
 *
 * Find a attribute  in the vehicle, or return %NULL if no
 * such attribute exists (in which case, @error is set to
 * %RSD_VEHICLE_ERROR_UNKNOWN_ATTRIBUTE).
 *
 * The caller is responsible for ensuring the attribute cache is up to date
 * before calling this function.
 *
 * Returns: (transfer none): the attribute
 * Since: 0.1.0
 */
static const RsdAttribute *
find_aggregate_attribute (VddAggregateVehicle        *self,
                          const gchar                *attribute_name,
                          const RsdAttributeMetadata **metadata,
                          GError                     **error)
{
  const RsdAttribute *attribute;
  const RsdAttributeMetadata *_metadata;

  /* Sanity check. */
  g_return_val_if_fail (self->cache_validity ==
                        (CACHE_VALUES_VALID | CACHE_STRUCTURE_VALID), NULL);

  /* Find the attribute. */
  attribute = vdd_aggregate_vehicle_get_attribute (self, attribute_name,
                                                   &_metadata);

  if (attribute == NULL)
    {
      g_set_error (error, RSD_VEHICLE_ERROR, RSD_VEHICLE_ERROR_UNKNOWN_ATTRIBUTE,
                   _("Unknown attribute  ‘%s’."), attribute_name);
      return NULL;
    }

  if (metadata != NULL)
    *metadata = _metadata;

  return attribute;
}

typedef struct
{
  /* Number of remaining incomplete rsd_vehicle_get_attributes_async()
   * calls. */
  gsize remaining;

  /* The monotonic timestamp when we started all the
   * rsd_vehicle_get_attribute_async() calls. This is in the aggregate clock
   * domain. */
  RsdTimestampMicroseconds local_start_time;
} UpdateCacheData;

static void
update_cache_check_if_finished (GTask  *task,
                                GError *error  /* transfer full */)
{
  VddAggregateVehicle *self;
  UpdateCacheData *data;
  gsize remaining;

  self = g_task_get_source_object (task);
  data = g_task_get_task_data (task);
  remaining = data->remaining;

  if ((remaining == 0 || error != NULL) && !g_task_had_error (task))
    {
      if (error == NULL)
        self->cache_validity = (CACHE_VALUES_VALID | CACHE_STRUCTURE_VALID);
      else
        self->cache_validity = CACHE_NOT_VALID;

      g_assert (self->cache_update_task == task);
      g_clear_object (&self->cache_update_task);

      if (error == NULL)
        g_task_return_boolean (task, TRUE);
      else
        g_task_return_error (task, error);
    }
  else if (error != NULL)
    {
      g_error_free (error);
    }
}

/* Update the clock delta for a given aggregated vehicle. The new delta is
 * calculated as the difference between the aggregate clock domain at the
 * //start// of an operation and the vehicle’s clock domain at the //end// of
 * the operation. This is not ideal, as we are trying to calculate the
 * difference between the two clocks at the same instant — but there is no way
 * of doing that with a vehicle on the other end of an IPC connection. So the
 * clock delta has noise contributions from the IPC round trip and the
 * processing time for the particular operation. Hopefully these will be small
 * compared to the constant offset between the two clock domains.
 *
 * The clock delta is only calculated once: if a clock delta already exists for
 * the given vehicle, it is unchanged. This eliminates the possibility of time
 * going backwards due to the noise; at the cost of potential clock skew.
 * However, since we’re not trying to synchronise the clocks – merely to make
 * them consistently comparable for the purposes of aggregating their
 * attributes – this should be fine. */
static gint64
update_vehicle_clock_delta (VddAggregateVehicle      *self,
                            RsdVehicle               *vehicle,
                            RsdTimestampMicroseconds  aggregate_start_time,
                            RsdTimestampMicroseconds  vehicle_current_time)
{
  gint64 *clock_delta;
  gint64 new_clock_delta;

  clock_delta = g_hash_table_lookup (self->clock_deltas, vehicle);

  if (aggregate_start_time == RSD_TIMESTAMP_UNKNOWN ||
      vehicle_current_time == RSD_TIMESTAMP_UNKNOWN)
    {
      DEBUG ("Skipping setting clock delta for vehicle ‘%s’ (%p) as one or "
             "both of the timestamps are unknown.",
             rsd_vehicle_get_id (vehicle), vehicle);

      if (clock_delta != NULL)
        return *clock_delta;
      else
        return 0;
    }

  new_clock_delta = aggregate_start_time - vehicle_current_time;

  if (clock_delta == NULL)
    {
      clock_delta = g_new0 (gint64, 1);
      *clock_delta = new_clock_delta;
      g_hash_table_insert (self->clock_deltas, vehicle, clock_delta);

      DEBUG ("Setting clock delta for vehicle ‘%s’ (%p) to %" G_GINT64_FORMAT "; "
             "aggregate_start_time = %" G_GINT64_FORMAT ", vehicle_current_time = "
             "%" G_GINT64_FORMAT ".",
             rsd_vehicle_get_id (vehicle), vehicle, *clock_delta,
             aggregate_start_time, vehicle_current_time);
    }
  else if (ABS (*clock_delta - new_clock_delta) > 500000  /* 500ms */)
    {
      DEBUG ("Clock delta for vehicle ‘%s’ (%p) seems to need skewing from %"
             G_GINT64_FORMAT " to %" G_GINT64_FORMAT ".",
             rsd_vehicle_get_id (vehicle), vehicle, *clock_delta,
             new_clock_delta);
    }

  return *clock_delta;
}

static void
update_cache_cb (GObject      *obj,
                 GAsyncResult *result,
                 gpointer      user_data)
{
  VddAggregateVehicle *self;
  RsdVehicle *vehicle = RSD_VEHICLE (obj);
  g_autoptr (GTask) task = G_TASK (user_data);
  g_autoptr (GPtrArray/*<owned RsdAttributeInfo>*/) attributes = NULL;
  GError *error = NULL;
  gsize i;
  RsdTimestampMicroseconds vehicle_start_time;
  UpdateCacheData *data;
  gint64 clock_delta;

  self = g_task_get_source_object (task);
  data = g_task_get_task_data (task);
  attributes = rsd_vehicle_get_attributes_finish (vehicle, result,
                                                  &vehicle_start_time,
                                                  &error);

  if (error != NULL)
    {
      /* Bail and leave the cache half-populated, with !CACHE_VALUES_VALID. */
      update_cache_check_if_finished (task, error);
      return;
    }

  /* Update the clock delta for this vehicle. */
  clock_delta = update_vehicle_clock_delta (self, vehicle,
                                            data->local_start_time,
                                            vehicle_start_time);

  /* Add attributes to the cache. There is also a more efficient way to do
   * this. */
  DEBUG ("Updating cache for vehicle ‘%s’ (%p).", rsd_vehicle_get_id (vehicle),
         vehicle);

  for (i = 0; i < attributes->len; i++)
    {
      RsdAttributeInfo *attribute_info = attributes->pdata[i];
      vdd_aggregate_vehicle_add_attribute (self, attribute_info, clock_delta);
    }

  data->remaining--;
  update_cache_check_if_finished (task, NULL);
}

static void vdd_aggregate_vehicle_update_cache_async  (VddAggregateVehicle  *self,
                                                       CacheValidity         required_validity,
                                                       GCancellable         *cancellable,
                                                       GAsyncReadyCallback   callback,
                                                       gpointer              user_data);

static void vdd_aggregate_vehicle_update_cache_finish (VddAggregateVehicle  *self,
                                                       GAsyncResult         *result,
                                                       GError              **error);

static void
update_cache_completed_second_attempt_cb (GObject      *source_object,
                                          GAsyncResult *result,
                                          gpointer      user_data)
{
  VddAggregateVehicle *self;
  g_autoptr (GTask) task = NULL;
  g_autoptr (GError) error = NULL;

  task = G_TASK (user_data);
  self = VDD_AGGREGATE_VEHICLE (source_object);

  vdd_aggregate_vehicle_update_cache_finish (self, result, &error);

  if (error != NULL)
    g_task_return_error (task, g_steal_pointer (&error));
  else
    g_task_return_boolean (task, TRUE);
}

static void
update_cache_completed_cb (GObject    *obj,
                           GParamSpec *pspec,
                           gpointer    user_data)
{
  g_autoptr (GTask) task = NULL;
  VddAggregateVehicle *self;
  CacheValidity required_validity;

  task = G_TASK (user_data);
  self = g_task_get_source_object (task);
  required_validity = GPOINTER_TO_UINT (g_task_get_task_data (task));

  if ((self->cache_validity & required_validity) == required_validity)
    {
      g_assert (self->cache_update_task == NULL);
      g_task_return_boolean (task, TRUE);
    }
  else if (self->cache_validity != CACHE_NOT_VALID)
    {
      /* The previous cache update operation had a different @required_validity.
       * Try again, this time with the validity we want.
       *
       * If the operation had failed completely, it would have completely
       * invalidated the cache. */
      vdd_aggregate_vehicle_update_cache_async (self, required_validity,
                                                g_task_get_cancellable (task),
                                                update_cache_completed_second_attempt_cb,
                                                g_object_ref (task));
    }
  else
    {
      /* This is currently the only failure case. */
      g_task_return_new_error (task, RSD_VEHICLE_ERROR,
                               RSD_VEHICLE_ERROR_UNKNOWN_ERROR,
                               _("Unknown error when querying vehicles."));
    }

  /* No need to disconnect the signal handler, because self->cache_update_task
   * will be finalised shortly. */
}

static void
vdd_aggregate_vehicle_update_cache_async (VddAggregateVehicle *self,
                                          CacheValidity        required_validity,
                                          GCancellable        *cancellable,
                                          GAsyncReadyCallback  callback,
                                          gpointer             user_data)
{
  g_autoptr (GTask) task = NULL;
  gsize i;
  UpdateCacheData *data;

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task, vdd_aggregate_vehicle_update_cache_async);

  if ((self->cache_validity & required_validity) == required_validity)
    {
      g_assert (self->cache_update_task == NULL);
      g_task_return_boolean (task, TRUE);
      return;
    }
  else if (self->cache_update_task != NULL)
    {
      /* An update is already pending; wait for it to finish. */
      g_task_set_task_data (task, GUINT_TO_POINTER (required_validity), NULL);
      g_signal_connect (G_OBJECT (self->cache_update_task), "notify::completed",
                        (GCallback) update_cache_completed_cb,
                        g_object_ref (task));
      return;
    }

  self->cache_update_task = g_object_ref (task);

  vdd_aggregate_vehicle_clear_attributes (self);

  data = g_new0 (UpdateCacheData, 1);
  data->remaining = 1;  /* to prevent premature completion */
  data->local_start_time = g_get_monotonic_time ();
  g_task_set_task_data (task, data, g_free);

  /* For each of our proxy vehicles, grab all its attributes and metadata.
   * FIXME: There is definitely a more efficient way to do this. */

  for (i = 0; i < self->vehicles->len; i++)
    {
      RsdVehicle *vehicle = RSD_VEHICLE (self->vehicles->pdata[i]);

      data->remaining++;

      rsd_vehicle_get_attributes_async (vehicle, "", cancellable,
                                        update_cache_cb,
                                        g_object_ref (task));
    }

  data->remaining--;
  update_cache_check_if_finished (task, NULL);
}

static void
vdd_aggregate_vehicle_update_cache_finish (VddAggregateVehicle  *self,
                                           GAsyncResult         *result,
                                           GError              **error)
{
  GTask *task = G_TASK (result);

  g_assert (g_task_is_valid (task, self));
  g_task_propagate_boolean (task, error);
}

static const gchar *
vdd_aggregate_vehicle_vehicle_get_id (RsdVehicle *vehicle)
{
  VddAggregateVehicle *self = VDD_AGGREGATE_VEHICLE (vehicle);

  /* We can’t simply grab the ID from the first vehicle, as we allow
   * rsd_vehicle_get_id() to be used after all vehicles have been removed from
   * the aggregate. */
  return self->id;
}

static void set_attributes_cb1 (GObject      *obj,
                               GAsyncResult *result,
                               gpointer      user_data);
static void set_attributes_cb2 (GObject      *obj,
                               GAsyncResult *result,
                               gpointer      user_data);

static void
vdd_aggregate_vehicle_vehicle_set_attributes_async (RsdVehicle          *vehicle,
                                                    GHashTable          *attribute_values,
                                                    GCancellable        *cancellable,
                                                    GAsyncReadyCallback  callback,
                                                    gpointer             user_data)
{
  VddAggregateVehicle *self = VDD_AGGREGATE_VEHICLE (vehicle);
  g_autoptr (GTask) task = NULL;

  task = g_task_new (vehicle, cancellable, callback, user_data);
  g_task_set_source_tag (task,
                         vdd_aggregate_vehicle_vehicle_set_attributes_async);

  g_task_set_task_data (task,
                        attribute_values,
                        NULL);

  /* Update the cache if needed. */
  vdd_aggregate_vehicle_update_cache_async (self, CACHE_VALID, cancellable,
                                            set_attributes_cb1,
                                            g_object_ref (task));
}

static void
set_attributes_cb1 (GObject      *obj,
                    GAsyncResult *result,
                    gpointer      user_data)
{
  VddAggregateVehicle *self = VDD_AGGREGATE_VEHICLE (obj);
  g_autoptr (GTask) task = G_TASK (user_data);
  const RsdAttribute *attribute;
  const RsdAttributeMetadata *metadata;
  gsize i;
  GError *error = NULL;
  gsize n_remaining;
  GHashTable *attributes = NULL;
  GHashTableIter iter;
  const char *attribute_name;
  GVariant *value = NULL;

  /* Check whether updating the cache was successful. */
  vdd_aggregate_vehicle_update_cache_finish (self, result, &error);

  if (error != NULL)
    {
      g_task_return_error (task, error);
      return;
    }

  /* Find the attribute. */
  attributes = g_task_get_task_data (task);
  g_hash_table_iter_init (&iter, attributes);
  
  while(g_hash_table_iter_next (&iter, (gpointer *)&attribute_name, 
                                (gpointer *)&value)) 
  {   
       attribute = find_aggregate_attribute (self,
                                             attribute_name,
                                             &metadata,
                                             &error);

       if (attribute == NULL)
       {
          g_task_return_error (task, error);
          return;
       }

       /* Check the attribute is available and writable. */
       if (metadata->availability != RSD_ATTRIBUTE_AVAILABLE)
       {
            g_task_return_new_error (task, RSD_VEHICLE_ERROR,
                                     RSD_VEHICLE_ERROR_ATTRIBUTE_UNAVAILABLE,
                                     _("Attribute ‘%s’ is unavailable."),
                                     attribute_name );
            return;
       }
       if (!(metadata->flags & RSD_ATTRIBUTE_WRITABLE))
       {
            g_task_return_new_error (task, RSD_VEHICLE_ERROR,
                                     RSD_VEHICLE_ERROR_ATTRIBUTE_NOT_WRITABLE,
                                     ("Attribute ‘%s’ is not writable."),
                                     attribute_name );
            return;
       }
    }
    /* Update the attribute’s value in each of the source vehicles.
     * TODO: Limit this to vehicles which provided the attribute and limit D-Bus
     * traffic. Notifications are sent out on the return path. Or maybe limit it
     * to a single vehicle.*/
    n_remaining = 1;
    g_task_set_task_data (task, GUINT_TO_POINTER (n_remaining), NULL);

    for (i = 0; i < self->vehicles->len; i++)
    {
         RsdVehicle *source_vehicle = self->vehicles->pdata[i];

         n_remaining++;
         rsd_vehicle_set_attributes_async (source_vehicle, 
                                           attributes,
                                           g_task_get_cancellable (task),
                                           set_attributes_cb2,
                                           g_object_ref (task));
    }

    n_remaining--;
    g_task_set_task_data (task, GUINT_TO_POINTER (n_remaining), NULL);
    if (GPOINTER_TO_UINT (g_task_get_task_data (task)) == 0)
      g_task_return_boolean (task, TRUE);
}

static void
set_attributes_cb2 (GObject      *obj,
                   GAsyncResult *result,
                   gpointer      user_data)
{
  RsdVehicle *source_vehicle = RSD_VEHICLE (obj);
  g_autoptr(GTask) task = user_data;
  GError *error = NULL;
  gsize n_remaining;

  rsd_vehicle_set_attributes_finish (source_vehicle, result, &error);

  n_remaining = GPOINTER_TO_UINT (g_task_get_task_data (task));
  n_remaining--;
  g_task_set_task_data (task, GUINT_TO_POINTER (n_remaining), NULL);

  if (!g_task_get_completed (task) && error != NULL)
    g_task_return_error (task, error);
  else if (error != NULL)
    g_error_free (error);
  else if (n_remaining == 0 && !g_task_get_completed (task))
    g_task_return_boolean (task, TRUE);
}

static void
vdd_aggregate_vehicle_vehicle_set_attributes_finish (RsdVehicle    *vehicle,
                                                    GAsyncResult  *result,
                                                    GError       **error)
{
  g_task_propagate_boolean (G_TASK (result), error);
}

static void get_attributes_cb (GObject      *obj,
                                   GAsyncResult *result,
                                   gpointer      user_data);

static void
vdd_aggregate_vehicle_vehicle_get_attributes_async (RsdVehicle          *vehicle,
                                                    const gchar         *node_path,
                                                    GCancellable        *cancellable,
                                                    GAsyncReadyCallback  callback,
                                                    gpointer             user_data)
{
  VddAggregateVehicle *self = VDD_AGGREGATE_VEHICLE (vehicle);
  g_autoptr (GTask) task = NULL;
  char *node = strdup(node_path);

  /* Should we invalidate the cache? Even if the cache claims to be valid, if
   * we are not subscribed to updates for this attribute, it might be out of
   * date. */
  if (!rsd_subscription_manager_has_subscription (self->subscriptions,
                                                  NULL))
    self->cache_validity &= ~CACHE_VALUES_VALID;

  task = g_task_new (vehicle, cancellable, callback, user_data);
  g_task_set_source_tag (task,
                         vdd_aggregate_vehicle_vehicle_get_attributes_async);
  g_task_set_task_data (task, node, g_free);

  /* Update cache if needed. */
  vdd_aggregate_vehicle_update_cache_async (self, CACHE_VALID, cancellable,
                                            get_attributes_cb,
                                            g_object_ref (task));
}

static void
get_attributes_cb (GObject      *obj,
                   GAsyncResult *result,
                   gpointer      user_data)
{
  VddAggregateVehicle *self = VDD_AGGREGATE_VEHICLE (obj);
  g_autoptr (GTask) task = G_TASK (user_data);
  GError *error = NULL;
  char* node_path = NULL; 
  g_autoptr (GPtrArray/*<owned RsdAttributeInfo>*/) attributes = NULL;

  /* Check whether updating the cache was successful. */
  vdd_aggregate_vehicle_update_cache_finish (self, result, &error);

  if (error != NULL)
    {
      g_task_return_error (task, error);
      return;
    }

  node_path  = g_task_get_task_data (task);

  /* Recurse to build the attribute array. */
  attributes = vdd_aggregate_vehicle_get_attributes (self, node_path);
  if(!attributes->len)
  {
      g_set_error (&error, RSD_VEHICLE_ERROR, RSD_VEHICLE_ERROR_UNKNOWN_ATTRIBUTE,
                   _("Unknown Attribute or branch ‘%s’."), node_path);
      g_task_return_error (task, g_steal_pointer (&error));
  }
  else
      timestamped_pointer_g_task_return (task, g_steal_pointer (&attributes),
                                        (GDestroyNotify) g_ptr_array_unref,
                                        g_get_monotonic_time ());
}

static GPtrArray/*<owned RsdAttributeInfo>*/ *
vdd_aggregate_vehicle_vehicle_get_attributes_finish (RsdVehicle                *vehicle,
                                                     GAsyncResult              *result,
                                                     RsdTimestampMicroseconds  *current_time,
                                                     GError                   **error)
{
  return timestamped_pointer_g_task_propagate (G_TASK (result),
                                               current_time, error);
}

static void get_metadata_cb (GObject      *obj,
                             GAsyncResult *result,
                             gpointer      user_data);

static void
vdd_aggregate_vehicle_vehicle_get_metadata_async (RsdVehicle           *vehicle,
                                                  const gchar          *node_path,
                                                  GCancellable         *cancellable,
                                                  GAsyncReadyCallback  callback,
                                                  gpointer             user_data)
{
  VddAggregateVehicle *self = VDD_AGGREGATE_VEHICLE (vehicle);
  g_autoptr (GTask) task = NULL;
  char* node = strdup(node_path);
  /* Should we invalidate the cache? Even if the cache claims to be valid, if
   * we are not subscribed to updates for this attribute, it might be out of
   * date. */
  if (!rsd_subscription_manager_has_subscription (self->subscriptions,
                                                  NULL))
    self->cache_validity &= ~CACHE_VALUES_VALID;

  task = g_task_new (vehicle, cancellable, callback, user_data);
  g_task_set_source_tag (task,
                         vdd_aggregate_vehicle_vehicle_get_metadata_async);
  g_task_set_task_data (task,  node, g_free);

  /* Update cache if needed. */
  vdd_aggregate_vehicle_update_cache_async (self, CACHE_VALID, cancellable,
                                            get_metadata_cb,
                                            g_object_ref (task));
}

static void
get_metadata_cb (GObject      *obj,
                 GAsyncResult *result,
                 gpointer      user_data)
{
  VddAggregateVehicle *self = VDD_AGGREGATE_VEHICLE (obj);
  g_autoptr (GTask) task = G_TASK (user_data);
  GError *error = NULL;
  char *node_path;
  g_autoptr (GPtrArray) metadatas= NULL;

  /* Check whether updating the cache was successful. */
  vdd_aggregate_vehicle_update_cache_finish (self, result, &error);

  if (error != NULL)
    {
      g_task_return_error (task, error);
      return;
    }

  node_path = g_task_get_task_data (task);

  /* Recurse to build the attribute array. */
  metadatas = vdd_aggregate_vehicle_get_metadata (self, node_path);
  timestamped_pointer_g_task_return (task, g_steal_pointer (&metadatas),
                                     (GDestroyNotify) g_ptr_array_unref,
                                     g_get_monotonic_time ());
}

static GPtrArray/*<owned RsdAttributeInfo>*/ *
vdd_aggregate_vehicle_vehicle_get_metadata_finish (RsdVehicle                *vehicle,
                                                   GAsyncResult              *result,
                                                   RsdTimestampMicroseconds  *current_time,
                                                   GError                   **error)
{
  return timestamped_pointer_g_task_propagate (G_TASK (result),
                                               current_time, error);
}

static void update_subscriptions_check_if_finished (GTask        *task,
                                                    GError       *error  /* transfer full */);
static void update_subscriptions_cb                (GObject      *obj,
                                                    GAsyncResult *result,
                                                    gpointer      user_data);

static void
vdd_aggregate_vehicle_vehicle_update_subscriptions_async (RsdVehicle          *vehicle,
                                                          GPtrArray           *subscriptions,
                                                          GPtrArray           *unsubscriptions,
                                                          GCancellable        *cancellable,
                                                          GAsyncReadyCallback  callback,
                                                          gpointer             user_data)
{
  VddAggregateVehicle *self = VDD_AGGREGATE_VEHICLE (vehicle);
  g_autoptr (GTask) task = NULL;
  gsize i;
  guint count;

  task = g_task_new (self, cancellable, callback, user_data);
  g_task_set_source_tag (task,
                         vdd_aggregate_vehicle_vehicle_update_subscriptions_async);

  /* Update the subscriptions for our own filters. */
  rsd_subscription_manager_update_subscriptions (self->subscriptions,
                                                 subscriptions,
                                                 unsubscriptions);

  /* Update all the vehicles we aggregate.
   * FIXME: This could be more intelligent in only propagating the subscriptions
   * which each vehicle would care about (according to the attributes it
   * provides) but this will do for now.
   *
   * We also need to check that all the subscriptions are for existing
   * attributes, otherwise we allow an attacker to allocate unbounded amounts of
   * memory by making repeated subscriptions to nonexistent attributes. */
  count = 1;  /* to prevent premature completion */
  g_task_set_task_data (task, GUINT_TO_POINTER (count), NULL);

  for (i = 0; i < self->vehicles->len; i++)
    {
      RsdVehicle *aggregated_vehicle = RSD_VEHICLE (self->vehicles->pdata[i]);

      count = GPOINTER_TO_UINT (g_task_get_task_data (task));
      g_task_set_task_data (task, GUINT_TO_POINTER (count + 1), NULL);

      rsd_vehicle_update_subscriptions_async (aggregated_vehicle,
                                              subscriptions, unsubscriptions,
                                              cancellable,
                                              update_subscriptions_cb,
                                              g_object_ref (task));
    }

  count = GPOINTER_TO_UINT (g_task_get_task_data (task));
  g_task_set_task_data (task, GUINT_TO_POINTER (count - 1), NULL);

  update_subscriptions_check_if_finished (task, NULL);
}

static void
update_subscriptions_check_if_finished (GTask  *task,
                                        GError *error  /* transfer full */)
{
  gsize remaining;

  remaining = GPOINTER_TO_UINT (g_task_get_task_data (task));

  if (remaining == 0 && error == NULL && !g_task_had_error (task))
    g_task_return_boolean (task, TRUE);
  else if (error != NULL && !g_task_had_error (task))
    g_task_return_error (task, error);
  else if (error != NULL)
    g_error_free (error);
}

static void
update_subscriptions_cb (GObject      *obj,
                         GAsyncResult *result,
                         gpointer      user_data)
{
  RsdVehicle *vehicle = RSD_VEHICLE (obj);
  g_autoptr (GTask) task = G_TASK (user_data);
  GError *error = NULL;
  gsize count;

  rsd_vehicle_update_subscriptions_finish (vehicle, result, &error);

  if (error != NULL)
    {
      /* Bail and leave the subscriptions half-updated. */
      update_subscriptions_check_if_finished (task, error);
      return;
    }

  count = GPOINTER_TO_UINT (g_task_get_task_data (task));
  g_task_set_task_data (task, GUINT_TO_POINTER (count - 1), NULL);

  update_subscriptions_check_if_finished (task, NULL);
}

static void
vdd_aggregate_vehicle_vehicle_update_subscriptions_finish (RsdVehicle    *vehicle,
                                                           GAsyncResult  *result,
                                                           GError       **error)
{
  GTask *task = G_TASK (result);

  g_task_propagate_boolean (task, error);
}

static gboolean
ptr_array_contains (GPtrArray *array,
                    gpointer   needle)
{
  gsize i;

  for (i = 0; i < array->len; i++)
    {
      if (array->pdata[i] == needle)
        return TRUE;
    }

  return FALSE;
}

/* Convert a timestamp from the clock domain of a vehicle to the aggregate
 * clock domain, using the @clock_delta taken from
 * VddAggregateVehicle.clock_deltas. */
static RsdTimestampMicroseconds
vehicle_time_to_aggregate_time (RsdTimestampMicroseconds vehicle_time,
                                gint64                   clock_delta)
{
  /* Unknown timestamps are invariant. */
  if (vehicle_time == RSD_TIMESTAMP_UNKNOWN)
    return vehicle_time;

  return vehicle_time + clock_delta;
}

static void
vehicle_attributes_changed_cb (RsdVehicle               *vehicle,
                               RsdTimestampMicroseconds  current_time,
                               GPtrArray                *changed_attributes,
                               GPtrArray                *invalidated_attributes,
                               gpointer                  user_data)
{
  VddAggregateVehicle *self = VDD_AGGREGATE_VEHICLE (user_data);
  RsdTimestampMicroseconds aggregate_current_time;
  gint64 clock_delta;

  /* Update the clock delta since the vehicle has just sent us its clock. */
  aggregate_current_time = g_get_monotonic_time ();
  clock_delta = update_vehicle_clock_delta (self, vehicle,
                                            aggregate_current_time,
                                            current_time);

  if (changed_attributes->len == 0 && invalidated_attributes->len == 0)
     return;

  /* Invalidate the cache and emit a signal. */
  self->cache_validity &= ~CACHE_VALUES_VALID;

  if (rsd_subscription_manager_is_subscribed (self->subscriptions,
                                              changed_attributes,
                                              invalidated_attributes,
                                              aggregate_current_time))
    {
      g_autoptr (GPtrArray/*<owned RsdAttributeInfo>*/) changed_attributes_updated = NULL;
      gsize i;

      /* We need to copy the attribute arrays so we can update the
       * timestamps to be in the right clock domain. */
      changed_attributes_updated = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_attribute_info_free);

      for (i = 0; i < changed_attributes->len; i++)
        {
          g_autoptr (RsdAttributeInfo) info = NULL;

          info = rsd_attribute_info_copy (changed_attributes->pdata[i]);
          info->attribute.last_updated = vehicle_time_to_aggregate_time (info->attribute.last_updated,
                                                                         clock_delta);

          g_ptr_array_add (changed_attributes_updated, g_steal_pointer (&info));
        }

      g_signal_emit_by_name (G_OBJECT (self), "attributes-changed",
                             vehicle_time_to_aggregate_time (current_time,
                                                             clock_delta),
                             changed_attributes_updated, invalidated_attributes);
    }
}

static void
vehicle_attributes_metadata_changed_cb (RsdVehicle               *vehicle,
                                        RsdTimestampMicroseconds  current_time,
                                        GPtrArray                *changed_attributes,
                                        gpointer                  user_data)
{
  VddAggregateVehicle *self = VDD_AGGREGATE_VEHICLE (user_data);
  RsdTimestampMicroseconds aggregate_current_time;
  gint64 clock_delta;

  /* Update the clock delta since the vehicle has just sent us its clock. */
  aggregate_current_time = g_get_monotonic_time ();
  clock_delta = update_vehicle_clock_delta (self, vehicle,
                                            aggregate_current_time,
                                            current_time);

  if (changed_attributes->len == 0)
    return;

  /* Invalidate the cache and emit a signal. */
  self->cache_validity &= ~CACHE_VALUES_VALID;

  g_signal_emit_by_name (G_OBJECT (self), "attributes-metadata-changed",
                         vehicle_time_to_aggregate_time (current_time,
                                                         clock_delta),
                         changed_attributes);
}

/**
 * vdd_aggregate_vehicle_update_vehicles:
 * @self: a #VddAggregateVehicle
 * @added: (nullable) (transfer none): #RsdVehicle instance to add to the
 *    aggregate, or %NULL to ignore
 * @removed: (nullable) (transfer none): #RsdVehicle instance to remove from
 *    the aggregate, or %NULL to ignore
 *
 * Update the set of vehicles in the aggregate, adding @added (if non-%NULL),
 * and removing @removed (if non-%NULL). If @added is the same as @removed, the
 * old #RsdVehicle instance for it will be removed from the
 * #VddAggregateVehicle and replaced with the new one from @added.
 *
 * The @added and @removed vehicles (if provided) must have the same ID as the
 * aggregate vehicle.
 *
 * Since: 0.1.0
 */
void
vdd_aggregate_vehicle_update_vehicles (VddAggregateVehicle *self,
                                       RsdVehicle          *added,
                                       RsdVehicle          *removed)
{
  gboolean changed = FALSE;

  g_return_if_fail (VDD_IS_AGGREGATE_VEHICLE (self));
  g_return_if_fail (added == NULL || RSD_IS_VEHICLE (added));
  g_return_if_fail (removed == NULL || RSD_IS_VEHICLE (removed));
  g_return_if_fail (added == NULL ||
                    g_strcmp0 (rsd_vehicle_get_id (added),
                               rsd_vehicle_get_id (RSD_VEHICLE (self))) == 0);
  g_return_if_fail (removed == NULL ||
                    g_strcmp0 (rsd_vehicle_get_id (removed),
                               rsd_vehicle_get_id (RSD_VEHICLE (self))) == 0);

  if (removed != NULL)
    {
      DEBUG ("Removing vehicle ‘%s’ (%p) from aggregate %p.",
             rsd_vehicle_get_id (removed), removed, self);

      g_object_ref (removed);  /* hold a reference while fiddling with it */
      changed = g_ptr_array_remove_fast (self->vehicles, removed);
      g_hash_table_remove (self->clock_deltas, removed);

      if (changed)
        g_signal_handlers_disconnect_by_data (removed, self);

      g_object_unref (removed);
    }

  if (added != NULL && !ptr_array_contains (self->vehicles, added))
    {
      g_autoptr (GPtrArray/*<owned RsdSubscription>*/) subscriptions = NULL;
      g_autoptr (GPtrArray/*<owned RsdSubscription>*/) unsubscriptions = NULL;

      DEBUG ("Adding vehicle ‘%s’ (%p) to aggregate %p.",
             rsd_vehicle_get_id (added), added, self);

      /* We deliberately don’t add to @clock_deltas here, as we don’t know the
       * initial delta. It’s done on the first call to
       * update_vehicle_clock_delta(). */
      g_ptr_array_add (self->vehicles, g_object_ref (added));
      changed = TRUE;

      g_signal_connect (added, "attributes-changed",
                        (GCallback) vehicle_attributes_changed_cb, self);
      g_signal_connect (added, "attributes-metadata-changed",
                        (GCallback) vehicle_attributes_metadata_changed_cb,
                        self);

      /* Update the subscriptions on the vehicle. */
      subscriptions = rsd_subscription_manager_get_subscriptions (self->subscriptions);
      unsubscriptions = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_subscription_free);

      if (subscriptions->len > 0)
        rsd_vehicle_update_subscriptions_async (added, subscriptions,
                                                unsubscriptions, NULL, NULL,
                                                NULL);
    }

  /* Changing the vehicles invalidates the cache structure as well as its
   * values. */
  if (changed)
    self->cache_validity = CACHE_NOT_VALID;
}

/**
 * vdd_aggregate_vehicle_get_vehicles:
 * @self: a #VddAggregateVehicle
 *
 * Get the set of vehicles currently in the aggregate.
 *
 * Returns: (transfer none) (element-type RsdVehicle): vehicles currently in the
 *    aggregate
 * Since: 0.1.0
 */
GPtrArray *
vdd_aggregate_vehicle_get_vehicles (VddAggregateVehicle *self)
{
  g_return_val_if_fail (VDD_IS_AGGREGATE_VEHICLE (self), NULL);

  return self->vehicles;
}

/**
 * vdd_aggregate_vehicle_new:
 * @id: vehicle ID
 *
 * Create a new, empty, #VddAggregateVehicle.
 *
 * Returns: (transfer full): a new aggregate vehicle
 * Since: 0.3.0
 */
VddAggregateVehicle *
vdd_aggregate_vehicle_new (const gchar *id)
{
  g_return_val_if_fail (rsd_vehicle_id_is_valid (id), NULL);

  return g_object_new (VDD_TYPE_AGGREGATE_VEHICLE,
                       "id", id,
                       NULL);
}

/**
 * vdd_aggregate_add_attribute:
 * @self: a #VddAggregateVehicle
 * @attribute_info: attribute information to add
 * @clock_delta: difference (in microseconds) between the vehicle’s clock and
 *    the aggregate clock domain, such that //adding// this delta to a timestamp
 *    from the vehicle gives a timestamp in the aggregate clock domain
 *
 * Add an attribute to the #VddAggregateVehicle
 *
 * Since: 0.4.0
 */
void
vdd_aggregate_vehicle_add_attribute (VddAggregateVehicle *self,
                                     RsdAttributeInfo    *attribute_info,
                                     gint64              clock_delta)
{
  RsdAttributeInfo attribute;
  RsdTimestampMicroseconds old_last_updated;

  g_return_if_fail (VDD_IS_AGGREGATE_VEHICLE (self));
  g_return_if_fail (attribute_info != NULL);  

  old_last_updated = attribute_info->attribute.last_updated;
  rsd_attribute_init (&attribute.attribute, &attribute_info->attribute);
  rsd_attribute_metadata_init (&attribute.metadata, &attribute_info->metadata);

 
  if (attribute.attribute.last_updated != RSD_TIMESTAMP_UNKNOWN)
    attribute.attribute.last_updated += clock_delta;

  DEBUG ("Attribute ‘%s’ set last_updated = %" G_GINT64_FORMAT " + %"
         G_GINT64_FORMAT " = %" G_GINT64_FORMAT ".",
         attribute_info->metadata.name, old_last_updated,
         clock_delta, attribute.attribute.last_updated);
  g_ptr_array_add (self->attributes, rsd_attribute_info_new(
                   &attribute.attribute, &attribute.metadata));

}


/**
 * vdd_aggregate_vehicle_get_attribute:
 * @self: a #VddAggregateVehicle
 * @attribute_name: name of the attribute to retrieve
 * @metadata: (out caller-allocates) (optional): return location for the
 *    attribute metadata
 *
 * Get the value of the given attribute from the aggregated vehicle. If no
 * attribute exists with that name, return %NULL.
 *
 * Returns: (nullable) (transfer none): attribute value, or %NULL if not found
 * Since: 0.1.0
 */
const RsdAttribute *
vdd_aggregate_vehicle_get_attribute (VddAggregateVehicle         *self,
                                     const gchar                 *attribute_name,
                                     const RsdAttributeMetadata  **metadata)
{
  gsize i;
  g_return_val_if_fail (VDD_IS_AGGREGATE_VEHICLE (self), NULL);

  /* Intern the @attribute_name for comparison. */
  attribute_name = g_intern_string (attribute_name);

  for (i = 0; i < self->attributes->len; i++)
    {
        RsdAttributeInfo *attribute =
              rsd_attribute_info_copy(self->attributes->pdata[i]); 

      if (!strcmp(attribute->metadata.name, attribute_name))
        {
          if (metadata != NULL)
            *metadata = &attribute->metadata;
          return &attribute->attribute;
        }
    }

  return NULL;
}

/**
 * vdd_aggregate_vehicle_clear_attributes:
 * @self: a #VddAggregateVehicle
 *
 * Clear all cached attributes.
 *
 * Since: 0.1.0
 */
void
vdd_aggregate_vehicle_clear_attributes (VddAggregateVehicle *self)
{
  g_return_if_fail (VDD_IS_AGGREGATE_VEHICLE (self));

  /* Clear from this vehicle. */
  if (self->attributes->len > 0)
  { 
    g_ptr_array_remove_range (self->attributes, 0, self->attributes->len);
  }
}

/**
 * vdd_aggregate_vehicle_get_all_metadata:
 * @self: a #VddAggregateVehicle
 *
 * Get metadata for all attributes.
 *
 * Returns: (element-type GVariant) (transfer container):
 *    potentially empty array of attribute metadata
 * Since: 0.2.0
 */
GPtrArray *
vdd_aggregate_vehicle_get_metadata (VddAggregateVehicle *self,
                                    gchar* node_path)
{
  g_autoptr (GPtrArray/*<owned RsdAttributeInfo>*/) metadatas = NULL;
  gsize i;
  gboolean wild_card  = FALSE;

  g_return_val_if_fail (VDD_IS_AGGREGATE_VEHICLE (self), NULL);

  metadatas = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_attribute_metadata_free);

  if(!strlen(node_path))
     wild_card = TRUE;

  for (i = 0; i < self->attributes->len; i++)
    {
      const RsdAttributeInfo *attribute;
      g_autoptr (RsdAttributeMetadata) metadata = NULL;

      attribute = rsd_attribute_info_copy(self->attributes->pdata[i]);
      if( (wild_card) || (!strncmp(node_path, attribute->metadata.name, strlen(node_path))))
      {
          metadata = rsd_attribute_metadata_copy (&attribute->metadata);
          g_ptr_array_add (metadatas, g_steal_pointer (&metadata));
      }
    }
   
  return g_steal_pointer (&metadatas);
}

/**
 * vdd_aggregate_vehicle_get_attributes:
 * @self: a #VddAggregateVehicle
 *
 * Get all attributes from this vehicle.
 *
 * Returns: (element-type RsdAttributeInfo) (transfer container): potentially
 *    empty array of attribute values
 * Since: 0.1.0
 */
GPtrArray *
vdd_aggregate_vehicle_get_attributes (VddAggregateVehicle *self,
                                      gchar               *node_path)
{
  g_autoptr (GPtrArray/*<owned RsdAttributeInfo>*/) attributes = NULL;
  gsize i;
  gboolean wild_card = FALSE;
  g_return_val_if_fail (VDD_IS_AGGREGATE_VEHICLE (self), NULL);

  attributes = g_ptr_array_new_with_free_func ((GDestroyNotify) rsd_attribute_info_free);

  if(!strlen(node_path))
     wild_card = TRUE;
  for (i = 0; i < self->attributes->len; i++)
    {
      RsdAttributeInfo *attribute; 
      attribute = self->attributes->pdata[i];
      if((wild_card) || (!strncmp(attribute->metadata.name, node_path, strlen(node_path))))
      {
          g_ptr_array_add (attributes, rsd_attribute_info_copy (attribute));
      }

    }
  return g_steal_pointer (&attributes);
}

