Rhosydd
=======

Rhosydd is a system service for handling access to sensors and actuators from
applications. It provides a vendor-specific hardware API to connect to the
services which control the hardware, and a SDK API for applications to use to
access it.

See the test programs in librhosydd/tests/ for simple examples of how to use
the code.

librhosydd’s API is currently unstable and is likely to change wildly.

Rhosydd is named after the Rhosydd quarry in Wales. It is pronounced ross-sith.

https://en.wikipedia.org/wiki/Rhosydd_Quarry
http://www.openstreetmap.org/way/269627097

Rhosydd also includes a library for use when writing backends and the daemon
itself, libcroesor. That is named after the Croesor quarry in Wales, which
was joined to Rhosydd.

https://en.wikipedia.org/wiki/Croesor_Quarry
http://www.openstreetmap.org/way/269618634

Design
======

See the Sensors and Actuators design for Apertis:

https://wiki.apertis.org/mediawiki/index.php/ConceptDesigns

Running Rhosydd
===============

systemctl start rhosydd.service

Deprecation guards
==================

If LIBRHOSYDD_DISABLE_DEPRECATED is defined when compiling against
librhosydd, all deprecated API will be removed from included headers.

Licensing
=========

Rhosydd is licensed under the MPLv2; see COPYING for more details.

Bugs
====

Bug reports and (git formatted) patches should be submitted to the Phabricator
instance for Apertis:

http://phabricator.apertis.org/

Contact
=======

See AUTHORS.
