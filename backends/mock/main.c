/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <glib.h>
#include <glib-unix.h>
#include <glib/gi18n.h>
#include <gio/gio.h>
#include <locale.h>
#include <signal.h>
#include <string.h>

#include "libcroesor/backend.h"
#include "libcroesor/static-vehicle.h"
#include "libcroesor/vehicle-manager.h"
#include "librhosydd/types.h"
#include "yaml.h"

GVariant *get_default_value (char *type);

static GPtrArray/*<owned RsdAttributeInfo>*/ *
yaml_to_attributes_array (const char *filename)
{
   FILE *fp = NULL;
   yaml_parser_t parser;
   yaml_event_t event;
   gboolean map_in_progress = FALSE;
   gboolean type_identified = FALSE;
   char *attribute_name = NULL;
   GVariant *attribute_value = NULL;
   GVariantBuilder builder;
   g_autoptr (GPtrArray/*<owned RsdAttributeInfo>*/) out = NULL;

   if (!filename || !strlen(filename))
     return NULL;

   if((fp = fopen(filename, "rb")) == NULL)
      return NULL;

   out = g_ptr_array_new_with_free_func (
         (GDestroyNotify) rsd_attribute_info_free);       

   yaml_parser_initialize (&parser);
   yaml_parser_set_input_file (&parser, fp);

   while ((yaml_parser_parse(&parser, &event)) && 
          (event.type != YAML_STREAM_END_EVENT) ) 
   {
        if (event.type == YAML_MAPPING_START_EVENT) 
        {
            //new attribute
            yaml_parser_parse(&parser, &event);
            attribute_name = strdup((const char *)
                             event.data.scalar.value);
            map_in_progress = TRUE; 
            g_variant_builder_init(&builder, G_VARIANT_TYPE("a{sv}"));
            yaml_parser_parse(&parser, &event);
            continue;
        } 
 
        if ((event.type == YAML_SCALAR_EVENT) && (map_in_progress)) 
        {
            char *value = NULL;
            char *key = NULL;
            key = (char *) malloc (event.data.scalar.length + 1);
            strncpy(key, (const char *)event.data.scalar.value,
                          event.data.scalar.length);
            key [event.data.scalar.length] =  '\0';
            
            yaml_parser_parse(&parser, &event); 
            value = (char *) malloc (event.data.scalar.length + 1);   
            strncpy(value, (const char *)event.data.scalar.value,
                            event.data.scalar.length);   
            value [event.data.scalar.length] =  '\0' ; 

            if( !strcmp(key, "type") && !strcmp(value, "branch")) 
                  map_in_progress = FALSE;     
            else if(!type_identified && !strcmp(key, "datatype"))
            {
               type_identified = TRUE;
               attribute_value = get_default_value (value);
               if (NULL == attribute_value)
                   map_in_progress = FALSE; 
            } 
            g_variant_builder_add (&builder, "{sv}", key,
                                      g_variant_new_string(value));
            free (key);
            free (value);
            continue;
        }

        if (event.type == YAML_MAPPING_END_EVENT)
        {
           GVariant *meta_info = NULL;
           meta_info = g_variant_builder_end (&builder);
           if (map_in_progress && type_identified)
           {
                RsdAttribute *attribute; 
                RsdAttributeMetadata *metadata;
                 
                attribute = rsd_attribute_new (attribute_value,
                                               0.0,
                                               g_get_monotonic_time()
                                            ); 
                metadata = rsd_attribute_metadata_new (attribute_name,
                                                       RSD_ATTRIBUTE_AVAILABLE,
                                                       RSD_ATTRIBUTE_READABLE,
                                                       meta_info);
                
                g_ptr_array_add (out, rsd_attribute_info_new(
                                 attribute, metadata )); 
           }
           map_in_progress = FALSE;
           type_identified = FALSE;    
           free (attribute_name);
        } 
     } //End of while 
     yaml_event_delete (&event);
     yaml_parser_delete (&parser);
     fclose (fp);
     return g_steal_pointer (&out);
}
 
GVariant* get_default_value ( char* data_type)
{
    if(!data_type || !strlen(data_type))
         return NULL;

    if(!strcmp(data_type, "uint8"))
       return g_variant_ref_sink(g_variant_new_byte(0));
    if(!strcmp(data_type, "int8"))
       return g_variant_ref_sink(g_variant_new_byte(0));
    if(!strcmp(data_type, "uint16"))
       return g_variant_ref_sink(g_variant_new_uint16(0));
    if(!strcmp(data_type, "int16"))
       return g_variant_ref_sink(g_variant_new_int16(0));
    if(!strcmp(data_type, "int32"))
       return g_variant_ref_sink(g_variant_new_int32(0));
    if(!strcmp(data_type, "uint32"))
       return g_variant_ref_sink(g_variant_new_uint32(0));
    if(!strcmp(data_type, "int64"))
       return g_variant_ref_sink(g_variant_new_int64(0));
    if(!strcmp(data_type, "uint64"))
       return g_variant_ref_sink(g_variant_new_uint64(0));
    if(!strcmp(data_type, "boolean"))
       return g_variant_ref_sink(g_variant_new_boolean(0));
    if(!strcmp(data_type, "float") || !strcmp(data_type, "double"))
       return g_variant_ref_sink(g_variant_new_double(0.0));
    if(!strcmp(data_type, "string"))
       return g_variant_ref_sink(g_variant_new_string("default"));
    return NULL;
}
  

int
main (int   argc,
      char *argv[])
{
  g_autoptr (GError) error = NULL;
  CsrVehicleManager *vehicle_manager;
  g_autoptr (CsrStaticVehicle) vehicle = NULL;
  g_autoptr (GPtrArray/*<unowned RsdVehicle>*/) added = NULL;
  g_autoptr (GPtrArray/*<owned RsdAttributeInfo>*/) props = NULL;
  g_autoptr (CsrBackend) backend = NULL;

  /* Localisation */
  bindtextdomain (GETTEXT_PACKAGE, LOCALEDIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
  textdomain (GETTEXT_PACKAGE);

  /* Create the backend. */
  backend = csr_backend_new ("org.apertis.Rhosydd1.Backends.Mock",
                             "/org/apertis/Rhosydd1/Backends/Mock",
                             GETTEXT_PACKAGE,
                             _("Export a mock vehicle object for use by the "
                               "vehicle device daemon."));
  vehicle_manager = csr_backend_get_vehicle_manager (backend);

  /* Add an example vehicle. */
  props = yaml_to_attributes_array ((const char *)MOCKBACKENDDIR"/vehicle_attributes_spec.yaml");
  printf("props len = %u", props->len);
  vehicle = csr_static_vehicle_new ("mock", props);

  added = g_ptr_array_new ();
  g_ptr_array_add (added, vehicle);
  csr_vehicle_manager_update_vehicles (vehicle_manager, added, NULL);

  /* Run until we are killed. */
  csr_service_run (CSR_SERVICE (backend), argc, argv, &error);

  if (error != NULL)
    {
      int code;

      if (g_error_matches (error,
                           CSR_SERVICE_ERROR, CSR_SERVICE_ERROR_SIGNALLED))
        raise (csr_service_get_exit_signal (CSR_SERVICE (backend)));

      g_printerr ("%s: %s\n", argv[0], error->message);
      code = error->code;

      return code;
    }

  return 0;
}

