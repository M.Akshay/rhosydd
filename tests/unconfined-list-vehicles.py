#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Copyright © 2016 Collabora Ltd.
#
# SPDX-License-Identifier: MPL-2.0
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

"""Integration test for the daemon to check unconfined access is denied.

This checks that a client process which is not confined by AppArmor is not
allowed to access the SDK sensors API. We expect to receive a D-Bus
ACCESS_DENIED error from the API when we try to use it. The underlying call
which is executed by the Rhosydd.VehicleManager constructor is a
GetManagedObjects call.
"""

import gi
from gi.repository import (GLib, Gio)

gi.require_version('Rhosydd', '0')
from gi.repository import Rhosydd  # pylint: disable=no-name-in-module


def _block_on_result_cb(source_object, async_result, user_data):
    """Callback which grabs and returns a GAsyncResult."""
    global result
    result = async_result


conn = Gio.bus_get_sync(Gio.BusType.SYSTEM, None)
main_context = GLib.main_context_default()
result = None

Rhosydd.VehicleManager.new_async(connection=conn,
                                 cancellable=None,
                                 callback=_block_on_result_cb,
                                 user_data=None)

while result is None:
    main_context.iteration(True)

try:
    Rhosydd.VehicleManager.new_finish(result)
except GLib.Error as err:
    if err.domain == GLib.quark_to_string(Gio.DBusError.quark()) and \
       err.code == Gio.DBusError.ACCESS_DENIED:
        print('Saw expected ACCESS_DENIED error.')
    else:
        raise AssertionError('Saw error (%s, %u) instead of expected '
                             'ACCESS_DENIED error. Error message was:\n%s' %
                             (err.domain, err.code, err.message))
else:
    raise AssertionError('Did not see expected ACCESS_DENIED error.')
