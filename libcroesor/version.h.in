/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef CSR_VERSION_H
#define CSR_VERSION_H

/**
 * CSR_MAJOR_VERSION:
 *
 * Evaluates to the major version of the libcroesor headers at compile time.
 * (e.g. in libcroesor version 1.2.3 this is 1).
 *
 * Since: 0.1.0
 */
#define CSR_MAJOR_VERSION (@CSR_VERSION_MAJOR@)

/**
 * CSR_MINOR_VERSION:
 *
 * Evaluates to the minor version of the libcroesor headers at compile time.
 * (e.g. in libcroesor version 1.2.3 this is 2).
 *
 * Since: 0.1.0
 */
#define CSR_MINOR_VERSION (@CSR_VERSION_MINOR@)

/**
 * CSR_MICRO_VERSION:
 *
 * Evaluates to the micro version of the libcroesor headers at compile time.
 * (e.g. in libcroesor version 1.2.3 this is 3).
 *
 * Since: 0.1.0
 */
#define CSR_MICRO_VERSION (@CSR_VERSION_MICRO@)

/**
 * CSR_CHECK_VERSION:
 * @major: major version (e.g. 1 for version 1.2.3)
 * @minor: minor version (e.g. 2 for version 1.2.3)
 * @micro: micro version (e.g. 3 for version 1.2.3)
 *
 * Evaluates to %TRUE if the version of the libcroesor header files
 * is the same as or newer than the passed-in version.
 *
 * Since: 0.1.0
 */
#define CSR_CHECK_VERSION(major,minor,micro) \
    (CSR_MAJOR_VERSION > (major) || \
     (CSR_MAJOR_VERSION == (major) && CSR_MINOR_VERSION > (minor)) || \
     (CSR_MAJOR_VERSION == (major) && CSR_MINOR_VERSION == (minor) && \
      CSR_MICRO_VERSION >= (micro)))

#endif /* !CSR_VERSION_H */
