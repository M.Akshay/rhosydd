/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef INTERNAL_ARRAYS_H
#define INTERNAL_ARRAYS_H

#include <glib.h>

G_BEGIN_DECLS

void ptr_array_uniqueify (GPtrArray    *array,
                          GCompareFunc  compare_func);

G_END_DECLS

#endif /* !INTERNAL_ARRAYS_H */
