/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <glib.h>
#include <locale.h>

#include "librhosydd/subscription.h"


/* Test that subscription construction works. */
static void
test_subscription_construction (void)
{
  g_autoptr (RsdSubscription) subscription = NULL;

  subscription = rsd_subscription_new ( "someAttribute", NULL, NULL, NULL,
                                       0, G_MAXUINT);

  g_assert_cmpstr (subscription->attribute_name, ==, "someAttribute");
  g_assert_null (subscription->minimum_value);
  g_assert_null (subscription->maximum_value);
  g_assert_cmpuint (subscription->minimum_period, ==, 0);
  g_assert_cmpuint (subscription->maximum_period, ==, G_MAXUINT);
}

/* Test that constructing a new #RsdSubscription which aggregates others
 * succeeds. */
static void
test_subscription_construction_aggregate (void)
{
  g_autoptr (RsdSubscription) subscription1 = NULL, subscription2 = NULL;
  g_autoptr (RsdSubscription) subscription3 = NULL, subscription4 = NULL;
  g_autoptr (RsdSubscription) subscription5 = NULL, subscription6 = NULL;
  g_autoptr (RsdSubscription) subscription7 = NULL, subscription8 = NULL;
  g_autoptr (RsdSubscription) subscription9 = NULL, subscription10 = NULL;
  g_autoptr (RsdSubscription) subscription11 = NULL, subscription12 = NULL;
  g_autoptr (RsdSubscription) subscription13 = NULL, subscription14 = NULL;
  g_autoptr (RsdSubscription) subscription15 = NULL;
  g_autoptr (RsdSubscription) subscription2plus3 = NULL, subscription1p = NULL;
  g_autoptr (RsdSubscription) subscription2plus3plus4 = NULL;
  g_autoptr (RsdSubscription) subscription4plus5 = NULL;
  g_autoptr (RsdSubscription) subscription1plus1p = NULL;
  g_autoptr (RsdSubscription) subscription5plus6 = NULL;
  g_autoptr (RsdSubscription) subscription14plus15 = NULL;
  gsize i;
  struct {
    RsdSubscription *subscriptions[3];
    gsize n_subscriptions;
    const RsdSubscription *expected_aggregate;
  } vectors[14];

  /* Build some subscriptions to use then plonk them in the test vector. */
  subscription1 = rsd_subscription_new ("*", NULL, NULL, NULL,
                                        0, G_MAXUINT);
  subscription1p = rsd_subscription_new ("*",
                                         g_variant_new_uint32 (0),
                                         g_variant_new_uint32 (50), NULL,
                                         5000, 50000);
  subscription2 = rsd_subscription_new ("someAttribute",
                                        g_variant_new_uint32 (0),
                                        g_variant_new_uint32 (50), NULL,
                                        5000, 50000);
  subscription3 = rsd_subscription_new ("someAttribute",
                                        g_variant_new_uint32 (10),
                                        g_variant_new_uint32 (40), NULL,
                                        5000, 40000);
  subscription4 = rsd_subscription_new ("someAttribute",
                                        g_variant_new_uint32 (10),
                                        g_variant_new_uint32 (60),
                                        g_variant_new_uint32 (5),
                                        5000, 60000);
  subscription5 = rsd_subscription_new ("someAttribute",
                                        g_variant_new_uint32 (10),
                                        g_variant_new_uint32 (60),
                                        g_variant_new_uint32 (3),
                                        5000, 60000);
  subscription6 = rsd_subscription_new ("someAttribute",
                                        g_variant_new_uint32 (10),
                                        g_variant_new_uint32 (60),
                                        g_variant_new_uint32 (3),
                                        70000, 80000);

  subscription7 = rsd_subscription_new ("someAttribute",
                                        g_variant_new_byte (10),
                                        g_variant_new_byte (60),
                                        g_variant_new_byte (3),
                                        0, G_MAXUINT);
  subscription8 = rsd_subscription_new ("someAttribute",
                                        g_variant_new_int16 (10),
                                        g_variant_new_int16 (60),
                                        g_variant_new_int16 (3),
                                        0, G_MAXUINT);
  subscription9 = rsd_subscription_new ("someAttribute",
                                        g_variant_new_int32 (10),
                                        g_variant_new_int32 (60),
                                        g_variant_new_int32 (3),
                                        0, G_MAXUINT);
  subscription10 = rsd_subscription_new ("someAttribute",
                                         g_variant_new_int64 (10),
                                         g_variant_new_int64 (60),
                                         g_variant_new_int64 (3),
                                         0, G_MAXUINT);
  subscription11 = rsd_subscription_new ("someAttribute",
                                         g_variant_new_uint16 (10),
                                         g_variant_new_uint16 (60),
                                         g_variant_new_uint16 (3),
                                         0, G_MAXUINT);
  subscription12 = rsd_subscription_new ("someAttribute",
                                         g_variant_new_uint64 (10),
                                         g_variant_new_uint64 (60),
                                         g_variant_new_uint64 (3),
                                         0, G_MAXUINT);
  subscription13 = rsd_subscription_new ("someAttribute",
                                         g_variant_new_double (10),
                                         g_variant_new_double (60),
                                         g_variant_new_double (3),
                                         0, G_MAXUINT);

  subscription14 = rsd_subscription_new ("someAttribute",
                                         g_variant_new_int32 (-10),
                                         g_variant_new_int32 (-5), NULL,
                                         0, G_MAXUINT);
  subscription15 = rsd_subscription_new ("someAttribute",
                                         g_variant_new_int32 (-15),
                                         g_variant_new_int32 (10), NULL,
                                         0, G_MAXUINT);

  subscription1plus1p = rsd_subscription_new ("*", NULL, NULL, NULL,
                                              5000, 50000);
  subscription2plus3 = rsd_subscription_new ("someAttribute",
                                             g_variant_new_uint32 (0),
                                             g_variant_new_uint32 (50), NULL,
                                             5000, 40000);
  subscription2plus3plus4 = rsd_subscription_new ("someAttribute",
                                                  g_variant_new_uint32 (0),
                                                  g_variant_new_uint32 (60),
                                                  NULL,
                                                  5000, 40000);
  subscription4plus5 = rsd_subscription_new ("someAttribute",
                                        g_variant_new_uint32 (10),
                                        g_variant_new_uint32 (60),
                                        g_variant_new_uint32 (3),
                                        5000, 60000);
  subscription5plus6 = rsd_subscription_new ("someAttribute",
                                             g_variant_new_uint32 (10),
                                             g_variant_new_uint32 (60),
                                             g_variant_new_uint32 (3),
                                             5000, 80000);
  subscription14plus15 = rsd_subscription_new ("someAttribute",
                                               g_variant_new_int32 (-15),
                                               g_variant_new_int32 (10), NULL,
                                               0, G_MAXUINT);

  /* Singleton. */
  vectors[0].subscriptions[0] = subscription1;
  vectors[0].n_subscriptions = 1;
  vectors[0].expected_aggregate = subscription1;

  /* Try anything with the wildcard subscription; should always return the
   * wildcard subscription. */
  vectors[1].subscriptions[0] = subscription1;
  vectors[1].subscriptions[1] = subscription1p;
  vectors[1].n_subscriptions = 2;
  vectors[1].expected_aggregate = subscription1plus1p;

  /* Try narrowing something. */
  vectors[2].subscriptions[0] = subscription2;
  vectors[2].subscriptions[1] = subscription3;
  vectors[2].n_subscriptions = 2;
  vectors[2].expected_aggregate = subscription2plus3;

  /* And narrowing it further. */
  vectors[3].subscriptions[0] = subscription3;
  vectors[3].subscriptions[1] = subscription2;
  vectors[3].subscriptions[2] = subscription4;
  vectors[3].n_subscriptions = 3;
  vectors[3].expected_aggregate = subscription2plus3plus4;

  /* Try narrowing hysteresis. */
  vectors[4].subscriptions[0] = subscription4;
  vectors[4].subscriptions[1] = subscription5;
  vectors[4].n_subscriptions = 2;
  vectors[4].expected_aggregate = subscription4plus5;

  /* Disjoint time periods. */
  vectors[5].subscriptions[0] = subscription5;
  vectors[5].subscriptions[1] = subscription6;
  vectors[5].n_subscriptions = 2;
  vectors[5].expected_aggregate = subscription5plus6;

  /* Various different types. */
  vectors[6].subscriptions[0] = subscription7;
  vectors[6].subscriptions[1] = subscription7;
  vectors[6].n_subscriptions = 2;
  vectors[6].expected_aggregate = subscription7;

  vectors[7].subscriptions[0] = subscription8;
  vectors[7].subscriptions[1] = subscription8;
  vectors[7].n_subscriptions = 2;
  vectors[7].expected_aggregate = subscription8;

  vectors[8].subscriptions[0] = subscription9;
  vectors[8].subscriptions[1] = subscription9;
  vectors[8].n_subscriptions = 2;
  vectors[8].expected_aggregate = subscription9;

  vectors[9].subscriptions[0] = subscription10;
  vectors[9].subscriptions[1] = subscription10;
  vectors[9].n_subscriptions = 2;
  vectors[9].expected_aggregate = subscription10;

  vectors[10].subscriptions[0] = subscription11;
  vectors[10].subscriptions[1] = subscription11;
  vectors[10].n_subscriptions = 2;
  vectors[10].expected_aggregate = subscription11;

  vectors[11].subscriptions[0] = subscription12;
  vectors[11].subscriptions[1] = subscription12;
  vectors[11].n_subscriptions = 2;
  vectors[11].expected_aggregate = subscription12;

  vectors[12].subscriptions[0] = subscription13;
  vectors[12].subscriptions[1] = subscription13;
  vectors[12].n_subscriptions = 2;
  vectors[12].expected_aggregate = subscription13;

  /* Test different signs. */
  vectors[13].subscriptions[0] = subscription14;
  vectors[13].subscriptions[1] = subscription15;
  vectors[13].n_subscriptions = 2;
  vectors[13].expected_aggregate = subscription14plus15;

  /* Run the tests. */
  for (i = 0; i < G_N_ELEMENTS (vectors); i++)
    {
      g_autoptr (GArray) subscriptions1 = NULL, subscriptions2 = NULL;
      g_autoptr (RsdSubscription) aggregate1 = NULL, aggregate2 = NULL;
      gsize j;

      g_test_message ("Vector %" G_GSIZE_FORMAT, i);

      subscriptions1 = g_array_new (FALSE, FALSE, sizeof (RsdSubscription));
      for (j = 0; j < vectors[i].n_subscriptions; j++)
        g_array_append_val (subscriptions1, *vectors[i].subscriptions[j]);

      aggregate1 = rsd_subscription_new_aggregate (subscriptions1);

      g_assert_true (rsd_subscription_equal (aggregate1,
                                             vectors[i].expected_aggregate));

      /* Re-run the test in reverse to check that the result is not
       * order-dependent. */
      g_test_message ("Vector %" G_GSIZE_FORMAT ", reversed", i);

      subscriptions2 = g_array_new (FALSE, FALSE, sizeof (RsdSubscription));
      for (j = 0; j < vectors[i].n_subscriptions; j++)
        g_array_append_val (subscriptions2,
                            *vectors[i].subscriptions[vectors[i].n_subscriptions - 1 - j]);

      aggregate2 = rsd_subscription_new_aggregate (subscriptions2);
      g_assert_true (rsd_subscription_equal (aggregate2,
                                             vectors[i].expected_aggregate));
    }
}

/* Test that copying a subscription works. */
static void
test_subscription_copy (void)
{
  g_autoptr (RsdSubscription) subscription1 = NULL, subscription2 = NULL;

  subscription1 = rsd_subscription_new ("someAttribute",
                                        g_variant_new_uint64 (0),
                                        g_variant_new_uint64 (5),
                                        g_variant_new_uint64 (2),
                                        0, G_MAXUINT / 2);
  subscription2 = rsd_subscription_copy (subscription1);

  g_assert_true (rsd_subscription_equal (subscription2, subscription1));
}

/* Test that matching a subscription against an attribute works. */
/* TODO: More tests, including hysteresis, large hysteresis values, time series,
 * attribute value ± accuracy which is wider than the value range, shifting the
 * attribute value from outside the hysteresis range to inside the range over a
 * long number of samples, massive shifts in consecutive samples, ensure the
 * first value outside the range is still signalled, signed ranges, floating
 * point ranges. */
static void
test_subscription_matches (void)
{
  const struct {
    struct {
      const gchar *minimum_value;
      const gchar *maximum_value;
      const gchar *hysteresis;
      guint minimum_period;
      guint maximum_period;
    } subscription;

    struct {
      const gchar *value;
      gdouble accuracy;
    } attribute;

    gboolean expect_match;
  } vectors[] = {
    { { NULL, NULL, NULL, 0, G_MAXUINT }, { "'hello'", 0.0 }, TRUE },
    { { NULL, NULL, NULL, 0, G_MAXUINT }, { "@u 6", 10.0 }, TRUE },
    { { "@u 1", "@u 10", NULL, 0, G_MAXUINT }, { "@u 6", 10.0 }, TRUE },
    { { "@u 1", "@u 10", NULL, 0, G_MAXUINT }, { "@u 6", 0.0 }, TRUE },
    { { "@u 1", "@u 10", NULL, 0, G_MAXUINT }, { "@u 11", 0.0 }, FALSE },
    { { "@u 1", "@u 10", NULL, 0, G_MAXUINT }, { "@u 0", 0.0 }, FALSE },
    { { "@u 10", "@u 10", NULL, 0, G_MAXUINT }, { "@u 10", 0.0 }, TRUE },
    { { "@u 10", "@u 12", NULL, 0, G_MAXUINT }, { "@u 13", 1.0 }, TRUE },
    { { "@u 10", "@u 12", NULL, 0, G_MAXUINT }, { "@u 9", 1.0 }, TRUE },
    { { "@u 12", "@u 10", NULL, 0, G_MAXUINT }, { "@u 9", 0.0 }, TRUE },
    { { "@u 12", "@u 10", NULL, 0, G_MAXUINT }, { "@u 11", 0.0 }, FALSE },
    { { "@u 12", "@u 10", NULL, 0, G_MAXUINT }, { "@u 11", 1.0 }, TRUE },
    { { "@u 12", "@u 10", NULL, 0, G_MAXUINT }, { "@u 13", 0.0 }, TRUE },
    { { "@u 12", "@u 10", NULL, 0, G_MAXUINT }, { "@u 9", 0.0 }, TRUE },

    /* Check other types: */
    { { "@y 12", "@y 10", NULL, 0, G_MAXUINT }, { "@y 9", 0.0 }, TRUE },
    { { "@n 12", "@n 10", NULL, 0, G_MAXUINT }, { "@n 9", 0.0 }, TRUE },
    { { "@q 12", "@q 10", NULL, 0, G_MAXUINT }, { "@q 9", 0.0 }, TRUE },
    { { "@i 12", "@i 10", NULL, 0, G_MAXUINT }, { "@i 9", 0.0 }, TRUE },
    { { "@x 12", "@x 10", NULL, 0, G_MAXUINT }, { "@x 9", 0.0 }, TRUE },
    { { "@t 12", "@t 10", NULL, 0, G_MAXUINT }, { "@t 9", 0.0 }, TRUE },
    { { "@d 12", "@d 10", NULL, 0, G_MAXUINT }, { "@d 9", 0.0 }, TRUE },
  };
  gsize i;

  for (i = 0; i < G_N_ELEMENTS (vectors); i++)
    {
      g_autoptr (RsdSubscription) subscription = NULL;
      g_autoptr (RsdAttribute) attribute = NULL;
      g_autoptr (GVariant) minimum_value = NULL, maximum_value = NULL;
      g_autoptr (GVariant) hysteresis = NULL;

      g_test_message ("Vector %" G_GSIZE_FORMAT " (expect match: %u): %s",
                      i, vectors[i].expect_match, vectors[i].attribute.value);

      if (vectors[i].subscription.minimum_value != NULL)
        minimum_value = g_variant_new_parsed (vectors[i].subscription.minimum_value);
      if (vectors[i].subscription.maximum_value != NULL)
        maximum_value = g_variant_new_parsed (vectors[i].subscription.maximum_value);
      if (vectors[i].subscription.hysteresis != NULL)
        hysteresis = g_variant_new_parsed (vectors[i].subscription.hysteresis);

      subscription = rsd_subscription_new ("someAttribute",
                                           g_steal_pointer (&minimum_value),
                                           g_steal_pointer (&maximum_value),
                                           g_steal_pointer (&hysteresis),
                                           vectors[i].subscription.minimum_period,
                                           vectors[i].subscription.maximum_period);
      attribute = rsd_attribute_new (g_variant_new_parsed (vectors[i].attribute.value),
                                   vectors[i].attribute.accuracy,
                                   0  /* last_updated */);

      if (vectors[i].expect_match)
        g_assert_true (rsd_subscription_matches (subscription, attribute, 0));
      else
        g_assert_false (rsd_subscription_matches (subscription, attribute, 0));
    }
}

/* Check that rsd_subscription_equal() works. */
static void
test_subscription_equal (void)
{
  g_autoptr (RsdSubscription) a = NULL, b = NULL, c = NULL, d = NULL;

  a = rsd_subscription_new ("someAttribute", NULL, NULL, NULL, 0,
                            G_MAXUINT);
  b = rsd_subscription_new ("someAttribute", NULL, NULL, NULL, 0,
                            G_MAXUINT);
  c = rsd_subscription_new ("someOtherAttribute", NULL, NULL, NULL, 0,
                            G_MAXUINT);
  d = rsd_subscription_new ("someAttribute", g_variant_new_uint32 (5),
                            g_variant_new_uint32 (15), g_variant_new_uint32 (1),
                            50, 5000);

  g_assert_true (rsd_subscription_equal (a, a));
  g_assert_true (rsd_subscription_equal (a, b));
  g_assert_false (rsd_subscription_equal (a, c));
  g_assert_false (rsd_subscription_equal (a, d));

  g_assert_true (rsd_subscription_equal (b, a));
  g_assert_true (rsd_subscription_equal (b, b));
  g_assert_false (rsd_subscription_equal (b, c));
  g_assert_false (rsd_subscription_equal (b, d));

  g_assert_false (rsd_subscription_equal (c, a));
  g_assert_false (rsd_subscription_equal (c, b));
  g_assert_true (rsd_subscription_equal (c, c));
  g_assert_false (rsd_subscription_equal (c, d));

  g_assert_false (rsd_subscription_equal (d, a));
  g_assert_false (rsd_subscription_equal (d, b));
  g_assert_false (rsd_subscription_equal (d, c));
  g_assert_true (rsd_subscription_equal (d, d));
}

int
main (int argc, char *argv[])
{
  setlocale (LC_ALL, "");

  g_test_init (&argc, &argv, NULL);

  g_test_add_func ("/subscription/construction",
                   test_subscription_construction);
  g_test_add_func ("/subscription/construction/aggregate",
                   test_subscription_construction_aggregate);
  g_test_add_func ("/subscription/copy", test_subscription_copy);
  g_test_add_func ("/subscription/matches", test_subscription_matches);
  g_test_add_func ("/subscription/equal", test_subscription_equal);

  return g_test_run ();
}
