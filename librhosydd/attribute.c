/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include <glib.h>
#include <string.h>

#include "librhosydd/attribute.h"


/**
 * rsd_attribute_info_new:
 * @attribute: value of the attribute
 * @metadata: metadata for the attribute
 *
 * Create a new #RsdAttributeInfo structure containing a copy of the given
 * @attribute value and its @metadata.
 *
 * Returns: (transfer full): a new #RsdAttributeInfo
 * Since: 0.1.0
 */
RsdAttributeInfo *
rsd_attribute_info_new  (const RsdAttribute         *attribute,
                         const RsdAttributeMetadata *metadata)
{
  g_autoptr (RsdAttributeInfo) info = NULL;

  g_return_val_if_fail (attribute != NULL, NULL);
  g_return_val_if_fail (metadata != NULL, NULL);
  g_return_val_if_fail (g_intern_string (metadata->name) ==
                        metadata->name, NULL);

  info = g_new0 (RsdAttributeInfo, 1);

  info->attribute.value = g_variant_ref (attribute->value);
  info->attribute.accuracy = attribute->accuracy;
  info->attribute.last_updated = attribute->last_updated;
  info->metadata.name = metadata->name;  /* guaranteed to be interned */
  info->metadata.availability = metadata->availability;
  info->metadata.flags = metadata->flags;
  info->metadata.vss_metadata = g_variant_ref(metadata->vss_metadata);
  return g_steal_pointer (&info);
}

/**
 * rsd_attribute_info_copy:
 * @info: (transfer none): a #RsdAttributeInfo
 *
 * Make a deep copy of an #RsdAttributeInfo.
 *
 * Returns: (transfer full): a copy of @info
 * Since: 0.1.0
 */
RsdAttributeInfo *
rsd_attribute_info_copy (const RsdAttributeInfo *info)
{
  return rsd_attribute_info_new (&info->attribute, &info->metadata);
}

/**
 * rsd_attribute_info_free:
 * @info: (transfer full): a #RsdAttributeInfo
 *
 * Free an #RsdAttributeInfo.
 *
 * Since: 0.1.0
 */
void
rsd_attribute_info_free (RsdAttributeInfo *info)
{
  g_return_if_fail (info != NULL);

  g_variant_unref (info->attribute.value);
  g_free (info);
}

G_DEFINE_BOXED_TYPE (RsdAttributeInfo, rsd_attribute_info,
                     rsd_attribute_info_copy, rsd_attribute_info_free)

/**
 * rsd_attribute_new:
 * @value: value of the attribute
 * @accuracy: standard deviation of the value, or 0.0 if unknown or @value is
 *    non-numeric
 * @last_updated: timestamp, in microseconds, the attribute was last measured
 *
 * Create a new #RsdAttribute structure containing a copy of the given
 * attribute @value and its metadata. If @value has a floating reference, it will
 * be sunk.
 *
 * Returns: (transfer full): a new #RsdAttribute
 * Since: 0.1.0
 */
RsdAttribute *
rsd_attribute_new  (GVariant     *value,
                    gdouble       accuracy,
                    RsdTimestampMicroseconds  last_updated)
{
  g_autoptr (RsdAttribute) attribute = NULL;

  g_return_val_if_fail (value != NULL, NULL);
  g_return_val_if_fail (g_variant_is_normal_form (value), NULL);
  g_return_val_if_fail (accuracy >= 0.0, NULL);

  attribute = g_new0 (RsdAttribute, 1);
  attribute->value = g_variant_ref_sink (value);
  attribute->accuracy = accuracy;
  attribute->last_updated = last_updated;

  return g_steal_pointer (&attribute);
}

/**
 * rsd_attribute_copy:
 * @attribute: (transfer none): a #RsdAttribute
 *
 * Make a deep copy of an #RsdAttribute.
 *
 * Returns: (transfer full): a copy of @attribute
 * Since: 0.1.0
 */
RsdAttribute *
rsd_attribute_copy (const RsdAttribute *attribute)
{
  return rsd_attribute_new (attribute->value, attribute->accuracy,
                            attribute->last_updated);
}

/**
 * rsd_attribute_init:
 * @attribute: attribute to initialise
 * @tmpl: (nullable): template to copy values from
 *
 * Initialise a pre-allocated #RsdAttribute structure, copying in
 * existing values from the given template. If the @attribute was allocated on
 * the heap, rsd_attribute_free() must be called on it once it’s finished with.
 * Otherwise, call rsd_attribute_clear().
 *
 * Returns: (transfer none): pointer to @attribute
 * Since: 0.1.0
 */
RsdAttribute *
rsd_attribute_init (RsdAttribute       *attribute,
                    const RsdAttribute *tmpl)
{
  g_return_val_if_fail (attribute != NULL, NULL);
  g_return_val_if_fail (tmpl != NULL, NULL);

  attribute->value = g_variant_ref (tmpl->value);
  attribute->accuracy = tmpl->accuracy;
  attribute->last_updated = tmpl->last_updated;

  return attribute;
}

/**
 * rsd_attribute_clear:
 * @attribute: (transfer none): a #RsdAttribute
 *
 * Clear the contents of an #RsdAttribute, but do not free the #RsdAttribute
 * itself.
 *
 * Since: 0.4.0
 */
void
rsd_attribute_clear (RsdAttribute *attribute)
{
  g_return_if_fail (attribute != NULL);

  g_clear_pointer (&attribute->value, g_variant_unref);
}

/**
 * rsd_attribute_free:
 * @attribute: (transfer full): a #RsdAttribute
 *
 * Free an #RsdAttribute.
 *
 * Since: 0.1.0
 */
void
rsd_attribute_free (RsdAttribute *attribute)
{
  g_return_if_fail (attribute != NULL);

  rsd_attribute_clear (attribute);
  g_free (attribute);
}

G_DEFINE_BOXED_TYPE (RsdAttribute, rsd_attribute,
                     rsd_attribute_copy, rsd_attribute_free)

/**
 * rsd_attribute_metadata_new:
 * @name: name of the attribute
 *
 * Create a new #RsdAttributeMetadata structure 
 *
 * Returns: (transfer full): a new #RsdAttributeMetadata
 * Since: 0.1.0
 */
RsdAttributeMetadata *
rsd_attribute_metadata_new (const gchar              *name,
                            RsdAttributeAvailability availability,
                            RsdAttributeFlags        flags,   
                            GVariant                 *vss_metadata)
{

  g_autoptr (RsdAttributeMetadata) l_metadata = NULL;

  g_return_val_if_fail (name != NULL && *name != '\0', NULL);
  g_return_val_if_fail (rsd_attribute_availability_is_valid (availability),
                        NULL);
  g_return_val_if_fail (rsd_attribute_flags_is_valid (flags), NULL);

  l_metadata = g_new0 (RsdAttributeMetadata, 1);

  l_metadata->name = g_intern_string (name);
  l_metadata->availability = availability;
  l_metadata->flags = flags;
  l_metadata->vss_metadata = g_variant_ref (vss_metadata);
  return g_steal_pointer (&l_metadata);
}

/**
 * rsd_attribute_metadata_copy:
 * @metadata: (transfer none): a #RsdAttributeMetadata
 *
 * Make a deep copy of a #RsdAttributeMetadata.
 *
 * Returns: (transfer full): a copy of @metadata
 * Since: 0.1.0
 */
RsdAttributeMetadata *
rsd_attribute_metadata_copy (const RsdAttributeMetadata *metadata)
{
  g_return_val_if_fail (metadata != NULL, NULL);

  return rsd_attribute_metadata_new (metadata->name,
                                     metadata->availability,
                                     metadata->flags,
                                     metadata->vss_metadata);
}

/**
 * rsd_attribute_metadata_init:
 * @metadata: attribute metadata to initialise
 * @tmpl: (nullable): template to copy values from, or %NULL to initialise
 *    to blank
 *
 * Initialise a pre-allocated #RsdAttributeMetadata structure, optionally copying
 * in existing values from the given template. If the @metadata was allocated on
 * the heap, rsd_attribute_metadata_free() must be called on it once it’s
 * finished with.
 *
 * Returns: (transfer none): pointer to @metadata
 * Since: 0.1.0
 */
RsdAttributeMetadata *
rsd_attribute_metadata_init (RsdAttributeMetadata       *metadata,
                             const RsdAttributeMetadata *tmpl)
{
  g_return_val_if_fail (metadata != NULL, NULL);

  if (tmpl != NULL)
    {
      metadata->name = tmpl->name;
      metadata->availability = tmpl->availability;
      metadata->flags = tmpl->flags;
      metadata->vss_metadata = g_variant_ref(tmpl->vss_metadata);
    }
  else
    {
      memset (metadata, 0, sizeof (*metadata));
    }

  return metadata;
}

/**
 * rsd_attribute_metadata_free:
 * @metadata: (transfer full): a #RsdAttributeMetadata
 *
 * Free an #RsdAttributeMetadata.
 *
 * Since: 0.1.0
 */
void
rsd_attribute_metadata_free (RsdAttributeMetadata *metadata)
{
  g_return_if_fail (metadata != NULL);
  g_variant_unref (metadata->vss_metadata);
  g_free (metadata);
}

G_DEFINE_BOXED_TYPE (RsdAttributeMetadata, rsd_attribute_metadata,
                     rsd_attribute_metadata_copy, rsd_attribute_metadata_free)

/**
 * rsd_attribute_name_is_valid:
 * @attribute_name: (nullable): a attribute name to validate
 *
 * Check whether the given @attribute_name is valid. %NULL is an allowed input,
 * and returns %FALSE.
 *
 * The format for attribute names is described in
 * [the Attributes documentation](#attribute-names).
 *
 * Returns: %TRUE if valid, %FALSE otherwise
 * Since: 0.1.0
 */
gboolean
rsd_attribute_name_is_valid (const gchar *attribute_name)
{
  if (attribute_name == NULL)
    return FALSE;

  return  g_regex_match_simple ("^([a-zA-Z][a-zA-Z0-9]*|[A-Z][A-Z0-9]*)"
                               "(\\.([a-zA-Z][a-zA-Z0-9]*|[A-Z][A-Z0-9]*))*$",
                               attribute_name, G_REGEX_DOLLAR_ENDONLY, 0);
}

/**
 * rsd_attribute_availability_is_valid:
 * @availability: an availability value to validate
 *
 * Check whether the given @availability is valid, only using defined values
 * from the #RsdAttributeAvailability enumerated type.
 *
 * Returns: %TRUE if valid, %FALSO otherwise
 * Since: 0.1.0
 */
gboolean
rsd_attribute_availability_is_valid (RsdAttributeAvailability availability)
{
  return (availability >= RSD_ATTRIBUTE_NOT_SUPPORTED &&
          availability <= RSD_ATTRIBUTE_NOT_SUPPORTED_OTHER);
}

/**
 * rsd_attribute_flags_is_valid:
 * @flags: a flags value to validate
 *
 * Check whether the given @flags is valid, only using defined values from the
 * #RsdAttributeFlags enumerated type, with all other bits zero.
 *
 * Returns: %TRUE if valid, %FALSE otherwise
 * Since: 0.1.0
 */
gboolean
rsd_attribute_flags_is_valid (RsdAttributeFlags flags)
{
  return ((flags & ~(RSD_ATTRIBUTE_READABLE | RSD_ATTRIBUTE_WRITABLE)) == 0);
}

